from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from flask import Flask, send_from_directory, send_file, request, jsonify
from selenium.webdriver.common.action_chains import ActionChains
from geopy import distance
from geopy.geocoders import Nominatim
import time

geolocator = Nominatim(user_agent="myapplication")
app = Flask(__name__)

@app.route("/<path:path>")
@app.route("/home/<path:path>")
def main(path):
    return send_from_directory("public", path)

@app.route("/")
@app.route("/home/")
def index():
    return send_file("./public/home.html")

@app.route("/input")
def input():
    return send_file("./public/input.html")

@app.route("/api")
def api():
    url = request.args.get('url', type=str) + "?th=1"
    loc = request.args.get('loc', type=str).lower().strip()
    out = {}
    
    driver = webdriver.Chrome()
    driver.get(url)

    try:
        WebDriverWait(driver, timeout=5).until(lambda d: d.find_element(By.ID, "captchacharacters"))
        
        time.sleep(13)
    except:
        pass

    try:
        el = WebDriverWait(driver, timeout=5).until(lambda d: d.find_element(By.ID, "productDetails_detailBullets_sections1"))
    
        weight = None
        origin = None

        for i in el.find_elements(By.CSS_SELECTOR, "table tr"):
            try:
                th = i.find_element(By.TAG_NAME, "th").text.strip().lower()
                td = i.find_element(By.TAG_NAME, "td").text.strip().lower()

                if th == "item weight":
                    weight = td
                if th == "country of origin":
                    origin = td
            except BaseException as e:
                continue

        driver.close()

        print(weight)
        print(origin)

        from_loc = geolocator.geocode(origin, exactly_one=True)
        to_loc = geolocator.geocode(loc, exactly_one=True)

        print(from_loc.longitude, from_loc.latitude)
        print(to_loc.longitude, to_loc.latitude)

        loc_distance = distance.great_circle((from_loc.latitude, from_loc.longitude), (to_loc.latitude, to_loc.longitude)).miles
        weight = float(weight[:weight.find(" ")])

        score = weight * loc_distance / 1000

        if weight is None and origin is not None:
            return jsonify({"code": "error", "message": "Could not get item weight"})
        if origin is None and weight is not None:
            return jsonify({"code": "error", "message": "Could not get country of origin"})
        if origin is None and weight is None:
            return jsonify({"code": "error", "message": "Could not get product info"})

        return jsonify({"code": "success", "weight": weight, "origin": origin, "distance": loc_distance, "score": score})
    except BaseException as e:
        print(e)
        driver.close()
        return jsonify({"code": "error", "message": "Could not get product info"})



