function getScore() {
    fetch(`api?loc=${document.getElementById("loc").value}&url=${document.getElementById("url").value}`).then(e => e.json()).then(e =>{
        document.getElementById("info_sheet").innerHTML = ""

        let scoreElm = document.createElement("h2")
        scoreElm.innerText = "Score: " + e.score
        document.getElementById("info_sheet").appendChild(scoreElm)

        for (const prop in e) {
            let elm = document.createElement("p")
            if (prop == "score") {
                continue
            }
            if (prop == "code") {
                continue
            }
            elm.innerText = `${prop}: ${e[prop]}`
            document.getElementById("info_sheet").appendChild(elm)
        }
    })
}